var timer = setInterval(function() {
    var blocker = document.querySelector("#blocker");
    if(document.querySelector(".view-go") != null) {
        blocker.remove();
    }
    if(document.querySelector(".view-score") != null) {
        clearInterval(timer);
    }
    if(document.querySelector(".view-waiting") != null) {
        if(blocker == null) {
            blocker = document.createElement('div');
            blocker.setAttribute("id", "blocker");
            blocker.style.position = "fixed";
            blocker.style.top = 0;
            blocker.style.left = 0;
            blocker.style.right = 0;
            blocker.style.bottom = 0;
            blocker.style.background = "rgba(0,0,0,.5)";
            document.querySelector("body").appendChild(blocker)
        }
    }
}, 1)