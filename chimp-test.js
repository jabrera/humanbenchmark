max = 0;
while(max <= 39) {
    buttons = {};
    max = 0;
    document.querySelector(".desktop-only").childNodes[0].childNodes[0].childNodes.forEach(row => {
        row.childNodes.forEach(cell => {
            if(cell.hasAttribute("data-cellnumber")) {
                cellnumber = parseInt(cell.getAttribute("data-cellnumber"))
                buttons[cellnumber] = cell;
                if(cellnumber > max) {
                    max = cellnumber;
                }
            }
        });
    });
    for(var i = 1; i <= max; i++) {
        buttons[i].click()
    }
    document.querySelector(".desktop-only").childNodes[2].querySelector("button").click();
}