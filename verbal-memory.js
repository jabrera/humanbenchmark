var memory = [];
function init() {
    var i = 0;
    var max = 1000;
    while(i <= max) {
        var word = document.querySelector(".word").textContent;
        if(memory.indexOf(word) == -1) {
            memory.push(word);
            document.querySelector(".verbal-memory-test").childNodes[0].childNodes[0].childNodes[0].childNodes[2].childNodes[2].click();
        } else {
            document.querySelector(".verbal-memory-test").childNodes[0].childNodes[0].childNodes[0].childNodes[2].childNodes[0].click();
        }
        i++;
    }
}

// Run this alone if you want more points
init();